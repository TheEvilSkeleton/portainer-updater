# Portainer Updater
Automatic updater for Portainer. The script checks for updates daily. If an update is available, it automatically updates. However this script does not remove the images. You need to manually remove these images.

## Installation
Run the command below as root:
```bash
bash <(curl -s https://gitlab.com/TheEvilSkeleton/portainer-updater/-/raw/main/install)
```

## Uninstallation
Run the command below as root:
```bash
bash <(curl -s https://gitlab.com/TheEvilSkeleton/portainer-updater/-/raw/main/uninstall)
```
